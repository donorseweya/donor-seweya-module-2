void main() {
  // creating object app
  var appname = "wumdrop";
  var appsector = "best android app";
  var appcategory = "interprise";
  var appdeveloper = "simon hartley";
  var appyear = 2015;

  print("app name is $appname");
  print("app sector is $appsector");
  print("app category is $appcategory");
  print("app developer is $appdeveloper");
  print("app year is $appyear");
}

// defining class
class app {
  var appname;
  var appsector;
  var appcategory;
  var appdeveloper;
  var appyear;
}

// class function
showappinfo() {
  print("app name is : ${'appname'}");
  print("app sector is : ${'appsector'}");
  print("app category is : ${'appcategory'}");
  print("app developer is : ${'appdeveloper'}");
  print("app year is : ${'appyear'}");
}
